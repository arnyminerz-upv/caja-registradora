import exception.IllegalOperationException;
import exception.IllegalTargetStateException;
import exception.UnknownUserException;
import exception.WrongPasswordException;
import security.Access;

import java.time.Instant;

public class CajaRegistra {
    private float euros;
    private int operator;

    /**
     * Stores a log of the operations made on the cash register. All operations are preceded by a key name of the following:
     * AUTH:
     */
    private String[] log = {};

    /**
     * Initializes the cash register, with a set amount of euros.
     *
     * @param startingEuros The starting amount of euros.
     */
    public CajaRegistra(float startingEuros) {
        euros = startingEuros;
        operator = -1;
    }

    /**
     * Initializes the cash register, with 0 euros.
     */
    public CajaRegistra() {
        this(0);
    }

    /**
     * Appends a new line in the log. This adds the time prefix automatically.
     *
     * @param line   The line to append.
     * @param format For filling placeholders.
     */
    private void appendLog(String line, Object... format) {
        // Add the time prefix
        String date = Instant.now().toString();
        line = date + "|" + line;

        // Format the line
        line = String.format(line, format);

        // Store the position where the log should be stored.
        int index = log.length;

        // Store the old log for reallocating the space.
        String[] oldLog = log;
        // Initialize log with the new size
        log = new String[index + 1];
        // Add again all the old values
        System.arraycopy(oldLog, 0, log, 0, index);
        // Add the new value in the array
        log[index] = line;
    }

    /**
     * Unlocks the cash register.
     *
     * @param username The name of the user to log.
     * @param password The password of the user to log.
     * @throws UnknownUserException   If the introduced user doesn't exist.
     * @throws WrongPasswordException If the introduced user exist but the password is wrong.
     * @throws IllegalStateException  If the cash register is already unlocked.
     */
    public void unlock(String username, String password) throws UnknownUserException, WrongPasswordException, IllegalStateException {
        if (!isLocked())
            throw new IllegalStateException("The cash is already unlocked. Operator: %d".formatted(operator));

        appendLog("ACC-UNLO:%s".formatted(username));
        try {
            operator = Access.authorise(username, password);
        } catch (UnknownUserException ex) {
            appendLog("AUTH-ERRU:%s", username);
            throw ex;
        } catch (WrongPasswordException ex) {
            appendLog("AUTH-ERRP:%s", username);
            throw ex;
        }
    }

    /**
     * Blocks the cash register.
     *
     * @throws IllegalStateException If the cash register is already locked.
     */
    public void lock() throws IllegalStateException {
        if (isLocked())
            throw new IllegalStateException("The cash is already locked");

        String name = Access.getUsername(operator);
        appendLog("ACC-LOCK:%s".formatted(name));

        operator = -1;
    }

    /**
     * Checks if the cash register is locked.
     *
     * @return Whether or not the cash register is locked.
     */
    public boolean isLocked() {
        return operator < 0;
    }

    /**
     * Inputs some money into the cash register.
     *
     * @param amount The amount of money to introduce.
     * @throws IllegalStateException    If the cash is locked.
     * @throws IllegalArgumentException If the amount of money is negative or 0.
     */
    public void moneyInput(float amount) throws IllegalStateException, IllegalArgumentException {
        if (isLocked()) {
            appendLog("ECO-IL:%f".formatted(amount));
            throw new IllegalStateException("The cash is locked.");
        }

        if (amount <= 0)
            throw new IllegalArgumentException("The amount to introduce is <= 0 (%f).".formatted(amount));

        appendLog("ECO-IN:%f".formatted(amount));
        euros += amount;
    }

    /**
     * Gets some money out of the cash register.
     *
     * @param amount The amount of money to extract.
     * @throws IllegalStateException     If the cash is locked.
     * @throws IllegalArgumentException  If the amount of money is negative or 0.
     * @throws IllegalOperationException When there's not enough money on the machine to extract.
     */
    public void moneyOutput(float amount) throws IllegalStateException, IllegalArgumentException, IllegalOperationException {
        if (isLocked()) {
            appendLog("ECO-OL:%f".formatted(amount));
            throw new IllegalStateException("The cash is locked.");
        }

        if (amount <= 0)
            throw new IllegalArgumentException("The amount to extract is <= 0 (%f).".formatted(amount));

        if (euros < amount) {
            appendLog("ECO-OE:%f;%f".formatted(euros, amount));
            throw new IllegalOperationException("Cannot extract %f euros from the machine since it only has %f euros.".formatted(amount, euros));
        }

        appendLog("ECO-OU:%f".formatted(amount));
        euros -= amount;
    }

    /**
     * Transfers money to the target machine
     *
     * @param amount The amount to transfer
     * @param other  The target machine
     * @throws IllegalOperationException   The budget of the current machine is not enough
     * @throws IllegalArgumentException    The amount to transfer is less or equal than 0
     * @throws IllegalStateException       The machine is locked
     * @throws IllegalTargetStateException The target machine is locked
     */
    public void transfer(float amount, CajaRegistra other)
            throws IllegalOperationException, IllegalArgumentException, IllegalStateException, IllegalTargetStateException {
        if (isLocked()) {
            appendLog("ECO-TL");
            throw new IllegalStateException("The cash is locked.");
        }

        if (amount <= 0)
            throw new IllegalArgumentException("The amount to extract is <= 0 (%f).".formatted(amount));

        String operatorName = getOperatorName();
        if (other.isLocked()) {
            appendLog("ECO-TT:%s".formatted(operatorName));
            throw new IllegalTargetStateException("The target cash is locked.");
        }

        if (euros < amount) {
            appendLog("ECO-TE:%f;%f".formatted(euros, amount));
            throw new IllegalOperationException("Cannot transfer %f euros from the machine since it only has %f euros.".formatted(amount, euros));
        }

        String targetOperatorName = other.getOperatorName();
        appendLog("ECO-TR:%f;%s;%s".formatted(amount, operatorName, targetOperatorName));
        moneyOutput(amount);
        other.moneyInput(amount);
    }

    /**
     * Gets the amount of euros there are in the cash register.
     *
     * @return The amount of euros in the cash register;
     * @throws IllegalStateException If the cash is locked.
     */
    public float getBudget() throws IllegalStateException {
        if (isLocked()) {
            appendLog("ECO-BL");
            throw new IllegalStateException("The cash is locked.");
        }

        return euros;
    }

    /**
     * Gets the log of the machine.
     *
     * @return The log lines of the machine.
     */
    public String[] getLog() {
        return log;
    }

    /**
     * Gets the log of the machine in a human-readable form.
     *
     * @return The log of the machine human-readable.
     */
    public String getReadableLog() {
        StringBuilder sBuilder = new StringBuilder();
        for (String line : getLog())
            sBuilder.append(StringFormatter.userReadableLog(line))
                    .append("\n");
        return sBuilder.toString();
    }

    /**
     * Gets the current operator's name
     *
     * @return The current operator's name
     * @throws IllegalStateException The machine is locked
     */
    public String getOperatorName() throws IllegalStateException {
        if (isLocked())
            throw new IllegalStateException("The cash is locked.");

        return Access.getUsername(operator);
    }
}
