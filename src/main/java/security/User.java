package security;

/**
 * A class for representing an user.
 *
 * @author Arnau Mora
 * @version 20210107
 */
public class User {
    /**
     * The name of the user used to log in.
     */
    private final String username;
    /**
     * The encrypted password for the user to log in.
     */
    private final String encPassword;

    /**
     * The salt for encrypting the user's password.
     */
    private final String salt;

    /**
     * Initializes the security.User class
     *
     * @param username The name of the user
     * @param password The password of the user
     */
    public User(String username, String password) {
        // Store the user's name raw
        this.username = username;

        // Generate a salt for the password
        salt = PasswordUtils.getSalt(32);
        encPassword = PasswordUtils.generateSecurePassword(password, salt);
    }

    /**
     * Specifies the result of a set validate operation.
     */
    public enum ValidationResult {
        OK,
        WRONG_USERNAME,
        WRONG_PASSWORD
    }

    /**
     * Validates an username and password combination for this user.
     *
     * @param username The name of the user.
     * @param password The password of the user.
     * @return OK if the user-password combination is correct.
     * WRONG_USERNAME if the username is incorrect.
     * WRONG_PASSWORD if the password is incorrect.
     */
    public ValidationResult validate(String username, String password) {
        if (!username.equals(this.username))
            return ValidationResult.WRONG_USERNAME;
        return PasswordUtils.verifyUserPassword(password, encPassword, salt) ?
                ValidationResult.OK :
                ValidationResult.WRONG_PASSWORD;
    }

    /**
     * Gets the user's name
     *
     * @return The user's name
     */
    public String getUsername() {
        return username;
    }
}
