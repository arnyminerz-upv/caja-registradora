package security;

import exception.UnknownUserException;
import exception.WrongPasswordException;

public class Access {
    /**
     * All the users allowed to log in, and their passwords.
     * TODO: A file-saving algorithm can be implemented to store the users in files, since the password is
     * encrypted inside.
     *
     * @see User
     */
    private static final User[] users = {
            new User("Juan", "juan00"),
            new User("Maria", "maria00"),
            new User("Andres", "andres00")
    };

    /**
     * Tries to authorise an user with a password.
     *
     * @param username The user to authorise.
     * @param password The password of the user.
     * @return The index of the authorised user.
     * @throws UnknownUserException   If the introduced user doesn't exist.
     * @throws WrongPasswordException If the introduced user exist but the password is wrong.
     */
    public static int authorise(String username, String password)
            throws UnknownUserException, WrongPasswordException {
        int c = 0;
        for (User user : users) {
            User.ValidationResult result = user.validate(username, password);

            // If WRONG_PASSWORD is returned, it means that the username is correct
            if (result == User.ValidationResult.WRONG_PASSWORD)
                throw new WrongPasswordException();

            if (result == User.ValidationResult.OK)
                return c;
            c++;
        }

        throw new UnknownUserException();
    }

    /**
     * Gets the name of the user at the set position.
     *
     * @param index The position to search
     * @return The name of the user at {index} position.
     * @throws IndexOutOfBoundsException If the specified index is out of the bounds of the users' list.
     */
    public static String getUsername(int index) throws IndexOutOfBoundsException {
        if (index >= users.length)
            throw new IndexOutOfBoundsException("The specified index %d is out of the users list bounds (<%d)".formatted(index, users.length));
        return users[index].getUsername();
    }
}
