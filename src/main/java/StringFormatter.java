public class StringFormatter {
    static class LogType {
        public String prefix;
        public int parameterCount;
        public String message;

        public LogType(String prefix, int parameterCount, String message) {
            this.prefix = prefix;
            this.parameterCount = parameterCount;
            this.message = message;
        }
    }

    private static final LogType[] logTypes = {
            new LogType("AUTH-ERRU", 2, "Intento de acceso de usuario desconocido: \"%s\"."),
            new LogType("AUTH-ERRP", 2, "Intento de acceso fraudulento: \"%s\"."),
            new LogType("AUTH-OK", 1, "Inicio de sesión correcto de \"%s\""),
            new LogType("ACC-LOCK", 1, "Caja bloqueada por %s"),
            new LogType("ACC-UNLO", 1, "Caja desbloqueada por %s"),
            new LogType("ECO-IN", 1, "Depósito: %f"),
            new LogType("ECO-IL", 1, "Intento de depósito de %f euros con caja bloqueda."),
            new LogType("ECO-OU", 1, "Retiro: %f"),
            new LogType("ECO-OE", 2, "Intento de retiro de %f euros con saldo insuficiente."),
            new LogType("ECO-OL", 1, "Intento de retiro de %f euros con caja bloqueada."),
            new LogType("ECO-TR", 3, "Transferencia de %f euros de %s a %s."),
            new LogType("ECO-TL", 0, "Intento de transferencia con caja bloqueada"),
            new LogType("ECO-TT", 1, "Intento de transferencia de %s con caja objetivo bloqueda."),
            new LogType("ECO-TE", 2, "Intento de transferencia con un saldo insuficiente de %f euros para %f euros."),
            new LogType("ECO-BL", 0, "Intento de obtención de balance de caja bloqueada")
    };

    private static String removeDate(String log) {
        return log.substring(log.indexOf('|') + 1);
    }

    /**
     * Returns an user-readable message for a log line.
     *
     * @param log The log line
     * @return The user-readable message
     */
    public static String userReadableLog(String log) {
        log = removeDate(log);

        for (LogType type : logTypes) {
            if (!log.startsWith(type.prefix))
                continue;
            String[] parameters = log.substring(log.indexOf(':') + 1).split(";");
            Object[] filler = new Object[type.parameterCount];
            for (int c = 0; c < type.parameterCount; c++) {
                if (c >= parameters.length)
                    break;
                String param = parameters[c];
                try {
                    filler[c] = Integer.parseInt(param);
                    continue;
                } catch (NumberFormatException ignored) {
                }
                try {
                    filler[c] = Float.parseFloat(param);
                    continue;
                } catch (NumberFormatException ignored) {
                }
                filler[c] = param;
            }
            return type.message.formatted(filler);
        }

        return "";
    }
}
