package exception;

public class UnknownUserException extends Exception {
    public UnknownUserException() {
        super("The user specified doesn't exist.");
    }
}
