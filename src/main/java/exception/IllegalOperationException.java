package exception;

public class IllegalOperationException extends Exception {
    public IllegalOperationException(String msg) {
        super(msg);
    }
}
