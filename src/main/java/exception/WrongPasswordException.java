package exception;

public class WrongPasswordException extends Exception {
    public WrongPasswordException() {
        super("The specified user-password combination is not correct");
    }
}
