package exception;

public class IllegalTargetStateException extends Exception {
    public IllegalTargetStateException(String msg) {
        super(msg);
    }
}
