import exception.IllegalOperationException;
import exception.IllegalTargetStateException;
import exception.UnknownUserException;
import exception.WrongPasswordException;

public class UsaCajas {
    private static final CajaRegistra cr1 = new CajaRegistra();
    private static final CajaRegistra cr2 = new CajaRegistra();

    public static void main(String[] args)
            throws WrongPasswordException, UnknownUserException, IllegalOperationException, IllegalTargetStateException {
        cr1.unlock("Juan", "juan00");
        cr1.moneyInput(50);
        cr1.moneyInput(100);
        cr1.moneyOutput(25);
        System.out.printf("The machine has %f euros.%n", cr1.getBudget());
        System.out.println("=== MACHINE 1 LOG ===");
        System.out.print(cr1.getReadableLog());

        try {
            cr2.unlock("Maria", "maria94");
        } catch (WrongPasswordException ignored) {
        }
        cr2.unlock("Maria", "maria00");

        cr1.transfer(100, cr2);

        cr1.lock();
        cr2.lock();

        System.out.println("=== MACHINE 1 LOG ===");
        System.out.print(cr1.getReadableLog());
        System.out.println();
        System.out.println("=== MACHINE 2 LOG ===");
        System.out.print(cr2.getReadableLog());
    }
}
