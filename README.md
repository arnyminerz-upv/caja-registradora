# Cash Register

This project is made to simulate a cash register.

## Cash Log

Each Cash Register ([Cash Register](/src/main/java/CajaRegistra.java)) has a String array, which stores all the
operations made on the machine.\
Each register starts with the time in ISO format.\
Each operation is preceded my a prefix from the following list. The prefix may have a value to inform the result of the
operation.

- [Authorisation (`AUTH`)](#auth)
- [Lock status (`ACC`)](#acc)
- [Money management (`ECO`)](#eco)

### `AUTH`

This identifies a login operation made on the machine, should be followed by a hyphen (`-`) and one of the following
codes, which identifies the operation type.

#### `AUTH-ERRU`

This shows a login attempt, with wrong username.\
Should be followed by two dots (`:`) and a string with the following format:
`username`.

**Example:**

```
2021-01-07T16:04:16.685394|AUTH-ERRU:Paco
```

#### `AUTH-ERRP`

This shows a login attempt, with wrong password.\
Should be followed by two dots (`:`) and a string with the following format:
`username`.

**Example:**

```
2021-01-07T16:04:16.685394|AUTH-ERR:Juan
```

#### `AUTH-OK`

This shows a login attempt, with wrong password.\
Should be followed by two dots (`:`), and the name of the user that has just logged in.

**Example:**

```
2021-01-07T16:04:16.685394|AUTH-OK:Juan
```

### `ACC`

This identifies a lock-unlock operation. Each line is followed by `LOCK`
(locked) or `UNLO` (unlocked) and two dots (`:`), followed by the name of the user that unlocked it.

**Example:**

```
2021-01-07T16:04:16.685394|ACC-UNLO:Juan
```

### `ECO`

This identifies an economic movement, must be followed by a hyphen (`-`) and one of the following codes, which
identifies the operation type.

#### `ECO-IN`

This shows money input for the specified machine.\
The line must end with two dots (`:`) followed by the amount of money introduced.

**Example:**

```
2021-01-07T16:04:16.685394|ECO-IN:50
```

#### `ECO-IL`

This shows an attempt to insert money from the machine, but it was locked.\
The line must end with two dots (`:`) followed by the amount of money wanted to be inserted.

**Example:**

```
2021-01-07T16:04:16.685394|ECO-IL:50
```

#### `ECO-OU`

This shows money output for the specified machine.\
The line must end with two dots (`:`) followed by the amount of money extracted.

**Example:**

```
2021-01-07T16:04:16.685394|ECO-OU:50
```

#### `ECO-OE`

This shows an attempt to extract money from the machine, but it didn't have enough budget.\
The line must end with two dots (`:`) followed by the amount of money wanted to be extracted, and the amount of budget
in the following format:
`budget;wanted`

**Example:**

```
2021-01-07T16:04:16.685394|ECO-OE:30;50
```

#### `ECO-OL`

This shows an attempt to extract money from the machine, but it was locked.\
The line must end with two dots (`:`) followed by the amount of money wanted to be extracted.

**Example:**

```
2021-01-07T16:04:16.685394|ECO-OL:50
```

#### `ECO-TR`

This shows a transfer of money between two unlocked machines.\
The line must end with two dots (`:`) followed by the amount of money moved and the source and target machines'
operators in the following format: `money;source;target`.

**Example:**

```
2021-01-07T16:04:16.685394|ECO-TR:50;Juan;Maria
```

#### `ECO-TL`

This shows an attempt to make a transfer from the machine, but it was locked.\

**Example:**

```
2021-01-07T16:04:16.685394|ECO-TL
```

#### `ECO-TT`

This shows an attempt to make a transfer from the machine, but the target machine was locked.\
The line must end with two dots (`:`) followed by the operator that tried to move the money.

**Example:**

```
2021-01-07T16:04:16.685394|ECO-TT:Juan
```

#### `ECO-TE`

This shows an attempt to transfer money from the machine, but it didn't have enough budget.\
The line must end with two dots (`:`) followed by the amount of money wanted to be transferred, and the amount of budget
in the following format:
`budget;wanted`

**Example:**

```
2021-01-07T16:04:16.685394|ECO-TE:30;50
```

#### `ECO-BL`

This shows an attempt to check the budget of the machine, but it was locked.\

**Example:**

```
2021-01-07T16:04:16.685394|ECO-BL
```
